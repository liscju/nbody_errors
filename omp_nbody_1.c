#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <malloc.h>
#include <math.h>

#include <omp.h>

/* stale */
#define GRAV_CONST 6.673e-11	/* m^3/(kg*s^2) */
#define NEWTON_OPT 3

#define X 0
#define Y 1
#define Z 2

/* struktura body */
struct Body_struct {
  double mass;
  double pos[3];
  double vel[3];
};

typedef struct Body_struct Body;

Body* init_bodies(unsigned int num_bodies);

int main(int argc, char **argv) {

  struct timeval timeStart, timeEnd;
  int num_bodies, num_steps, max_threads=1;
  int i, j, k, l;
  double dt = 1.0, dv[3];
  double r[3], dist, force_len, force_ij[3], tot_force_i[3];

  double *forces_matrix;

  Body *bodies;

  /* przetw�rz argumenty programu */
  if (argc < 2) {
    fprintf(stderr, "Wrong number of input parameters");
    exit(1);
  }

  num_bodies = atoi(argv[1]);
  num_steps = atoi(argv[2]);

  max_threads = omp_get_max_threads();

  /**************************************************************************
   * Inicjacja bodies oraz struktur danych
   *************************************************************************/

  printf("Inicjalizacja z %d watkami, %d bodies, %d krokami\n",
		  max_threads, num_bodies, num_steps);

  bodies = init_bodies(num_bodies);

  #if NEWTON_OPT == 1
  forces_matrix = (double *) malloc(sizeof(double)*2*num_bodies*num_bodies);
  #elif NEWTON_OPT == 2
  forces_matrix = (double *) malloc(sizeof(double)*2*num_bodies);
  #endif

  /* poczatek odmierzania czasu */
  printf("Running "); fflush(stdout);
  printf("\n");

  gettimeofday(&timeStart, NULL);

  /**************************************************************************
   * gl�wna petla
   *************************************************************************/

#define PRIVATE_VARS r, dist, force_len, force_ij, tot_force_i, dv

/* Store in force_ij[] the force on body i by body j */
#define Calc_Force_ij() \
  r[X] = bodies[j].pos[X] - bodies[i].pos[X]; \
  r[Y] = bodies[j].pos[Y] - bodies[i].pos[Y]; \
  dist = r[X]*r[X] + r[Y]*r[Y]; \
  force_len = GRAV_CONST * bodies[i].mass * bodies[j].mass  \
    / (dist*sqrt(dist)); \
  force_ij[X] = force_len * r[X]; \
  force_ij[Y] = force_len * r[Y]

/* Update velocity and position of body i, by numerical integration */
#define Step_Body_i() \
  dv[X] = dt * tot_force_i[X] / bodies[i].mass; \
  dv[Y] = dt * tot_force_i[Y] / bodies[i].mass; \
  bodies[i].pos[X] += dt * ( bodies[i].vel[X] + dv[X]/2 ); \
  bodies[i].pos[Y] += dt * ( bodies[i].vel[Y] + dv[Y]/2 ); \
  bodies[i].vel[X] += dt * dv[X]; \
  bodies[i].vel[Y] += dt * dv[Y]

  for (k=0; k<num_steps; k++) {

  #if NEWTON_OPT == 0
    #pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
      tot_force_i[X] = 0.0;
      tot_force_i[Y] = 0.0;

      /* Compute total force f(i) on each body i */
      for (j=0; j<num_bodies; j++) {
	if (j==i) continue;

	Calc_Force_ij();

	tot_force_i[X] += force_ij[X];
	tot_force_i[Y] += force_ij[Y];
      }

      Step_Body_i();
    }

  #elif NEWTON_OPT == 1

    #define forces(i,j,x) forces_matrix[x + 2*(j + num_bodies*i)]

    /* Fill in a big nxn table of forces */
#pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
      for (j=i+1; j<num_bodies; j++) {
        Calc_Force_ij();

	forces(i, j, X) = force_ij[X];
	forces(i, j, Y) = force_ij[Y];
      }
    }

    /* Compute total force f(i) on each body i */
#pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
      tot_force_i[X] = 0.0;
      tot_force_i[Y] = 0.0;

      for (j=0; j<i; j++) {
        tot_force_i[X] -= forces(j, i, X);
        tot_force_i[Y] -= forces(j, i, Y);
      }
      for (j=i+1; j<num_bodies; j++) {
        tot_force_i[X] += forces(i, j, X);
        tot_force_i[Y] += forces(i, j, Y);
      }

      /* Update velocity and position of each body, by numerical integration */
      Step_Body_i();
    }

  #elif NEWTON_OPT == 2

    #define forces(i,x) forces_matrix[x + 2*i]

#pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
      for (j=i+1; j<num_bodies; j++) {
        Calc_Force_ij();

#pragma omp critical
        {
          forces(i,X) += force_ij[X];
          forces(i,Y) += force_ij[Y];
	  forces(j,X) -= force_ij[X];
	  forces(j,Y) -= force_ij[Y];
	}
      }
    }

#pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
      Step_Body_i();
    }

    #elif NEWTON_OPT == 3

    #define forces(i,x) forces_matrix[x + 2*i]

    omp_lock_t *lock = (omp_lock_t *)malloc(num_bodies * sizeof(omp_lock_t));

#pragma omp parallel for private(j, PRIVATE_VARS), schedule(static, 1)
    for (i=0; i<num_bodies; i++) {
      for (j=i+1; j<num_bodies; j++) {
        Calc_Force_ij();

          omp_set_lock(&lock[i]);
          forces(i,X) += force_ij[X];
          forces(i,Y) += force_ij[Y];
          omp_unset_lock(&lock[i]);

          omp_set_lock(&lock[j]);
	  forces(j,X) -= force_ij[X];
	  forces(j,Y) -= force_ij[Y];
	  omp_unset_lock(&lock[j]);
      }
    }

#pragma omp parallel for private(j, PRIVATE_VARS)
    for (i=0; i<num_bodies; i++) {
      Step_Body_i();
    }  

  #endif /* !NEWTON_OPT */

  }

  /* Stop timing */
  gettimeofday(&timeEnd, NULL);

  int time = (timeEnd.tv_sec - timeStart.tv_sec) * 1000 + (timeEnd.tv_usec - timeStart.tv_usec) / 1000;

  printf("Czas %d\n ms", time);

  printf("done!  interaction rate: \n%6.0f\n",
    num_bodies * (num_bodies-1) * num_steps /
    num_bodies);

  return(0);
}


/*Alokacja pamieci i inicjalizacja obiektow*/
Body *init_bodies(unsigned int num_bodies) {
  int i;
  double n = num_bodies;
  Body *bodies = (Body *)malloc(num_bodies * sizeof(Body));

  for (i=0; i<num_bodies; i++) {
      bodies[i].mass = 1.0;
      bodies[i].pos[X] = i/n;
      bodies[i].pos[Y] = i/n;
      bodies[i].vel[X] = 0.0;
      bodies[i].vel[Y] = 0.0;
  }

  return bodies;
}

